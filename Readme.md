# Yoyo Weather API
## Dependences
- Django
- Rest Framework
- Requests
- django-cors-headers

## Run with Docker
The project is ready to be built and deployed using Docker. Issue the following commands to build the image and start it.
- `docker build -t yoyoweather`
- `docker run --name yoyoweather -d -p 8000:8000 yoyoweather`

## Data update daemon
Updated weather data is fetched from OpenWeatherMap every time the service is queried for a city and the available data is older than an hour.
In order to retrieve fresh data in a timely manner, a fetch new data daemon has been written. It can be launched using this command: `python manage.py fetchnewdata`.  